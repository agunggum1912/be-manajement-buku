<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function login(Request $request) {
        $rule = [
            'email'     => 'required|email',
            'password'  => 'required'
        ];

        $validate = Validator::make($request->all(), $rule);
        if($validate->fails())
            return $this->json400($validate->errors()->first());

        if (!$token = auth()->attempt($validate->validated()))
            return $this->json400('Invalid credentials.');

        $data = $this->createNewToken($token);

        return $this->json200($data, 'Login successfully!');
    }

    public function register(Request $request){
        $rule = [
            'email'     => 'required|string|email|unique:App\Models\User,email',
            'password'  => 'required|string|confirmed|min:6||regex:/[0-9]/|regex:/[a-z]/|regex:/[A-Z]/|regex:/[\W]/'
        ];
        
        $validate = Validator::make($request->all(), $rule, [
            'password.regex' => 'The password must contain numbers, lowercase, uppercase letters and special characters.',
        ]);
        
        if ($validate->fails())
            return $this->json400($validate->errors()->first());

        DB::beginTransaction();
        try {
            $user = new User();
            $user->email    = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();

            DB::commit();
            return $this->json201(null, "Create user successfully.");
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->json400($e->getMessage());
        }
    }

    public function logout() {
        auth()->logout();
        return $this->json200(null, 'Successfully logged out.');
    }

    public function get_profile() {
        $profile = auth()->user();
        return $this->json200($profile, 'Get profile successfully.');
    }

    public function refresh(){
        $data = $this->createNewToken(auth()->refresh());
        return $this->json200($data, 'Refresh token successfully.');
    }

    protected function createNewToken($token) {
        $user = Auth()->user();
        return [
            'id'            => encrypt($user->id),
            'email'         => $user->email,
            'token'         => $token,
            'token_type'    => 'bearer',
            'expires_in'    => auth()->factory()->getTTL() * 60,
        ];
    }
}
