<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BukuController extends Controller
{
    public function datatable(Request $request) {
        $rule = [
            'page'      => 'nullable|integer',
            'per_page'  => 'nullable|integer',
            'search'    => 'nullable|string',
            'order_by'  => 'nullable|in:title,stok',
            'sort'      => 'nullable|in:desc,asc',
        ];

        $validator = Validator::make($request->all(), [
            'sort.required'     => 'Key sort not found.',
            'order_by.required' => 'Order by not found',
        ]);

        if ($validator->fails())
            return $this->json400($validator->errors()->first());

        $search     = $request->search;
        $order_by   = $request->order_by ?: 'title';
        $sort       = $request->sort ?: 'asc';
        $page       = (int) $request->page ?: 1;
        $per_page   = (int) $request->per_page ?: 10;
        $offset     = ($page - 1) * $per_page;

        $columns =  [
            'title' => 'judul',
            'stok'  => 'stok',
        ];

        $query = Buku::select('id', 'judul', 'stok', 'gambar');

        if ($search) {
            $query = $query->where(function ($q) use ($columns, $search) {
                foreach ($columns as $value)
                    $q->orWhere($value, 'like', "%$search%");
            });
        }

        $clone      = clone $query;
        $get_data   = $query->orderBy($columns[$order_by], $sort)->offset($offset)->limit($per_page)->get();

        $list = [];
        foreach ($get_data as $value) {
            $list[] = [
                'id'        => encrypt($value->id),
                'title'     => $value->judul,
                'stok'      => $value->stok,
                'gambar'    => asset('storage/img/buku/') . '/' . $value->gambar,
            ];
        }
        $data = [
            'total'     => $clone->count(),
            'data_list' => $list
        ];

        return $this->json200($data, "Get data book successfully.");
    }

    public function store(Request $request) {
        $rule = [
            'title'     => 'required',
            'stok'      => 'required|integer',
            'gambar'    => 'required|file|mimes:jpg,jpeg,png'
        ];

        $validate = Validator::make($request->all(), $rule);
        if($validate->fails())
            return $this->json400($validate->errors()->first());

        $timestamp = date("YmdHis");
        DB::beginTransaction();
        try {
            $file_name          = $request->title . '_' . $timestamp;
            $file_upload        = $request->file('gambar');
            $file_ext           = strtolower($file_upload->getClientOriginalExtension());
            $file_name_output   = $file_name . '.' . $file_ext;
            $file_upload->storeAs('public/img/buku', $file_name_output);

            $buku = new Buku();
            $buku->judul        = $request->title;
            $buku->stok         = $request->stok;
            $buku->gambar       = $file_name_output;
            $buku->created_by   = Auth()->user()->id;
            $buku->save();

            DB::commit();
            return $this->json201(null, 'Create book successfully.');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->json400($e->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        $check = $this->checkDecrypt([$id]);
        if ($check)
            return $this->json400($check->original['message']);

        $rule = [
            'title'     => 'required',
            'stok'      => 'required|integer',
            'gambar'    => 'nullable|file|mimes:jpg,jpeg,png'
        ];

        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails())
            return $this->json400($validate->errors()->first());

        $timestamp = date("YmdHis");
        DB::beginTransaction();
        try {
            $buku = Buku::find(decrypt($id));
            if(!$buku)
                return $this->json400('Data not found.');

            if ($request->hasFile('gambar')) {
                $file_name          = $request->title . '_' . $timestamp;
                $file_upload        = $request->file('gambar');
                $file_ext           = strtolower($file_upload->getClientOriginalExtension());
                $file_name_output   = $file_name . '.' . $file_ext;
                $file_upload->storeAs('public/img/buku', $file_name_output);
                $buku->gambar       = $file_name_output;
            }

            $buku->judul        = $request->title;
            $buku->stok         = $request->stok;
            $buku->created_by   = Auth()->user()->id;
            $buku->save();

            DB::commit();
            return $this->json201(null, 'Update book successfully.');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->json400($e->getMessage());
        }
    }

    public function delete($id) {
        $check = $this->checkDecrypt([$id]);
        if ($check)
            return $this->json400($check->original['message']);

        $data = Buku::find(decrypt($id));
        if(!$data)
            return $this->json400('Data not found.');

        $data->delete();
        return $this->json400("Book deleted successfully.");        
    }
}
