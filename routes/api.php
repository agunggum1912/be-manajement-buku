<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::controller('AuthController')
    ->group(function($g) {
        $g->post('/login','login')->name('login');
        $g->post('/register', 'register')->name('register');
    });
    
Route::middleware('jwt')->group(function($jwt) {
    $jwt->controller('AuthController')->group(function($g) {
        $g->post('/logout', 'logout')->name('logout');
        $g->get('/get_profile', 'get_profile')->name('get_profile');
        $g->get('/refresh', 'refresh')->name('refresh');
    });

    $jwt->name('buku.')
        ->prefix('buku')
        ->controller('BukuController')
        ->group(function($g) {
            $g->get('/datatable', 'datatable')->name('datatable');
            $g->post('/add', 'store')->name('store');
            $g->post('/edit/{id}', 'update')->name('update');
            $g->get('/detail/{id}', 'detail')->name('detail');
            $g->delete('/delete/{id}', 'delete')->name('delete');
    });
});
